/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.xxxxxxx;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author tud08
 */
public class OXprojectTest {
    
    public OXprojectTest() {
    }



    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
     @Test
     public void testCheckVerticleWinCol1() {
         char table[][] = {{'O', '-', '-'}, 
                           {'O', '-', '-'}, 
                           {'O', '-', '-'}};
         char currentPlayer = 'O';
         int col=1;
         assertEquals(false, OX.checkVertical(table,currentPlayer,col));
     }
     @Test
     public void testCheckVerticleWinCol2() {
         char table[][] = {{'-', 'X', '-'}, 
                           {'-', 'X', '-'}, 
                           {'-', 'X', '-'}};
         char currentPlayer = 'X';
         int col=2;
         assertEquals(true, OX.checkVertical(table,currentPlayer,col));
     }
     @Test
     public void testCheckVerticleWinCol3() {
         char table[][] = {{'-', '-', 'X'}, 
                           {'-', '-', 'X'}, 
                           {'-', '-', 'X'}};
         char currentPlayer = 'X';
         int col=3;
         assertEquals(false, OX.checkVertical(table,currentPlayer,col));
     }
     @Test
     public void testCheckHoriZontalWinRow1() {
         char table[][] = {{'O', 'O', 'O'}, 
                           {'-', '-', '-'}, 
                           {'-', '-', '-'}};
         char currentPlayer = 'O';
         int row=1;
         assertEquals(true, OX.checkVertical(table,currentPlayer,row));
     }
     @Test
     public void testCheckHoriZontalWinRow2() {
         char table[][] = {{'-', '-', '-'}, 
                           {'O', 'O', 'O'}, 
                           {'-', '-', '-'}};
         char currentPlayer = 'O';
         int row=2;
         assertEquals(false, OX.checkVertical(table,currentPlayer,row));
     }
     @Test
     public void testCheckHoriZontalWinRow3() {
         char table[][] = {{'X', 'X', 'X'}, 
                           {'-', '-', '-'}, 
                           {'-', '-', '-'}};
         char currentPlayer = 'O';
         int row=1;
         assertEquals(false, OX.checkVertical(table,currentPlayer,row));
     }
     @Test
     public void testCheckDiagonalWin112233() {
         char table[][] = {{'O', '-', '-'}, 
                           {'-', 'O', '-'}, 
                           {'-', '-', 'O'}};
         char currentPlayer = 'O';
         assertEquals(true, OX.checkX1(table,currentPlayer));
     }
     @Test
     public void testCheckDiagonalWin132231() {
         char table[][] = {{'-', '-', 'O'}, 
                           {'-', 'X', '-'}, 
                           {'X', '-', '-'}};
         char currentPlayer = 'X';
         assertEquals(false, OX.checkX1(table,currentPlayer));
     }
     @Test
     public void testCheckDraw() {
         char table[][] = {{'X', 'O', 'O'}, 
                           {'O', 'X', 'X'}, 
                           {'O', 'X', 'O'}};
         int count = 9;
         assertEquals(true, OX.checkDraw(count));
     }
     
     
}
